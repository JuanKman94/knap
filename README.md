# Knapsack problem

C implementation of solution for the binary knapsack problem, just for fun

To generate random knapsack problem instances build the rigger:

```
$ gcc rigger.c
$ ./a.out 20 10 20 5 10
$ cat kp-20.dat
```

## Metaheuristic
At this point I've stumbled a problem because of my design choices.
If, instead of using `item` and `sack` structures, I had used a list of items
as a solution, e.g., a linked list and evaluated lists for feasability,
implementation of metaheuristics would be simpler (and faster) because now I've
got a huge copy overhead when trying a modified feasible solution.
