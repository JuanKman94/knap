# Compiler
CC = gcc

# flags for compiler
# 	-g: debug info to executable object
# 	-Wall: most compiler's warnings enabled
CFLAGS = -g -Wall

PROG = lists

# build target
TARGET = lists

.PHONY: run clean run-rigger

default: run

build: clean main.c
	$(CC) $(CFLAGS) -o $(TARGET) main.c

build-rigger: rig.c rigger.c
	$(CC) $(CFLAGS) -o rigger rigger.c

rigger: build-rigger
	./rigger 20 25 50 20 30

run: build
	./$(TARGET) kp-20.dat sol-20.dat

meta: run
	./$(TARGET) sol-20.dat

clean:
	$(RM) $(TARGET)
