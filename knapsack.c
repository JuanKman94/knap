#ifndef KNAP
#define KNAP 1

#include <string.h>

#if defined(KNAP_DEBUG)
#define dmesg(str) printf("%s: %s\n", "DEBUG", str)
#else
#define dmesg(str)
#endif

extern int AVG_KNAP_SIZE;

void set_avg_knap_size(int new)
{
	AVG_KNAP_SIZE = new;
}

struct item new_item(int w, int v)
{
	struct item i = { .w = w, .v = v };
	return i;
}

struct sack *new_sack()
{
	struct sack *_sack = NULL;

	_sack = (struct sack*) malloc(sizeof(struct sack));

	if (_sack != NULL) {
		_sack->n = 0, _sack->w = 0, _sack->v = 0;
		_sack->size = AVG_KNAP_SIZE;
	}

	dmesg("sack created");
	return _sack;
}

int add_sack_item(struct sack *sack, struct item *item)
{
	void *tmp = NULL;
	struct item _item;

	tmp = realloc(sack->items, sizeof(struct item) * (sack->n + 1) );

	if (tmp != NULL) {
		_item.w = item->w, _item.v = item->v;
		sack->items = tmp;
		sack->items[sack->n] = _item;

		sack->n++;
		sack->w += item->w;
		sack->v += item->v;

		dmesg("item added to sack");
		return 0;
	}

	perror("add_sack_item: realloc()");
	return 1;
}

void sort_val_desc(struct item *items, int n)
{
	struct item tmp;
	int i, j;

	for (i = 0; i < n; i++) {
		for (j = i+1; j < n; j++) {
			if (items[j].v > items[i].v) {
				tmp = items[i];
				items[i] = items[j];
				items[j] = tmp;
			}
		}
	}
}

void sort_val_asc(struct item *items, int n)
{
	struct item tmp;
	int i, j;

	for (i = 0; i < n; i++) {
		for (j = i+1; j < n; j++) {
			if (items[j].v < items[i].v) {
				tmp = items[i];
				items[i] = items[j];
				items[j] = tmp;
			}
		}
	}
}

void sort_weight_desc(struct item *items, int n)
{
	struct item tmp;
	int i, j;

	for (i = 0; i < n; i++) {
		for (j = i+1; j < n; j++) {
			if (items[j].w > items[i].w) {
				tmp = items[i];
				items[i] = items[j];
				items[j] = tmp;
			}
		}
	}
}

void sort_weight_asc(struct item *items, int n)
{
	struct item tmp;
	int i, j;

	for (i = 0; i < n; i++) {
		for (j = i+1; j < n; j++) {
			if (items[j].w < items[i].w) {
				tmp = items[i];
				items[i] = items[j];
				items[j] = tmp;
			}
		}
	}
}

void print_items(struct item *items, int n)
{
	int i;

	puts("ITEM\tWEIGHT\tVALUE");
	for (i = 0; i < n; i++) {
		printf("%2d:\t%2d\t%2d\n", i+1, items[i].w, items[i].v);
	}
}

void print_sack(struct sack *_sack)
{
	if (_sack != NULL) {
		printf("==== Sack ====\n"
				"n: %d\tw: %d\tv: %d\tsize: %ld\n",
				_sack->n,
				_sack->w,
				_sack->v,
				_sack->size
			  );

		print_items(_sack->items, _sack->n);
	}
}

#endif
