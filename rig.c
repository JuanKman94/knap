#ifndef RIG
#define RIG 1

#include <stdio.h>
#include <stdlib.h>
#include "knapsack.c"

#define MAX_NAME 32

int rand_int(int min, int max)
{
	// srand needs to be called previously
	return min + rand() % (max+1 - min);
}

int get_knapsack_W(int N, int w_min, int w_max)
{
	return ( N * ( (w_min + w_max) / 2) ) / 2;
}

/* Read problem instance file with the format
 * 	<N> <W>
 * 	<w> <v>	// N rows
 *
 *  Where <N> is the quantity of items for selection,
 *  <W> the knapsack capacity and
 *  <w> & <v> individual weight and value for each item
 *
 * 	return 0 on success
 */
int read_instance(char *fname, int *N, int *W, struct item **items)
{
	int z,	// status
		i,
		_w, _v;
	struct item *tmp;
	FILE *f;

	// open file
	f = fopen(fname, "r");
	if (f == NULL) {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	// read basic instance properties
	z = fscanf(f, "%d %d", N, W);
	if (z != 2) {
		fprintf(stderr, "Error reading file. Expected 2 values, got %d\n", z);
		return 1;
	}

	// allocate memory for array of items
	*items = calloc(*N, sizeof(struct item)); // array of items
	if (!items) {
		perror("calloc");
		exit(EXIT_FAILURE);
	}

	// parse items
	for(i = 0; i < *N; i++) {
		tmp = &(*items)[i];
		z = fscanf(f, "%d %d", &_w, &_v);

		if (z != 2) {
			fprintf(stderr, "Error parsing file, line %d\n", i + 2);
			_w = 0, _v = 0;
		}

		*tmp = new_item(_w, _v);
	}

	fclose(f);
	return 0;
}

void write_instance(int N, int w_min, int w_max, int v_min, int v_max)
{
	int W,
		i;
	char name[MAX_NAME];
	FILE *f;

	// open output file
	snprintf(name, MAX_NAME, "kp-%d.dat", N);
	f = fopen(name, "w");
	if (f == NULL) {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	// generate W
	W = get_knapsack_W(N, w_min, w_max);

	// begin writing file
	fprintf(f, "%d %d\n", N, W);
	for (i = 0; i < N; i++) {
		fprintf(f, "%d %d\n", rand_int(w_min, w_max), rand_int(v_min, v_max));
	}

	// close output file
	fclose(f);
	printf("Instance written to %s\n", name);
}

/**
 * Read solution file with the format
 * <n> <w> <v> <size>
 * <w> <v> // size rows
 * <N> <W>
 * <w> <v> // N rows
 *
 * @paramin fname char*
 * @paramout N int*, quantity of item structs filled in items
 * @paramout W int*, maximum capacity allowed for solutions
 * @paramout sol struct sack**, filled from file fname
 * @paramout items struct item**, filled from file fname
 *
 * Return 0 on success
 */
int read_solution(char *fname, int *N, int *W, struct sack **sol, struct item **items)
{
	int z,
		i,
		_w, _v;
	FILE *f = fopen(fname, "r");
	struct item *sol_items = NULL,
				*tmp;
	struct sack *s = NULL;

	if (f == NULL) {
		fprintf(stderr, "read_solution: fopen()\n");
		goto err;
	}

	// allocate memory for solution
	s = malloc(sizeof(struct sack));
	if (s == NULL) {
		fprintf(stderr, "read_solution: malloc()\n");
		goto err;
	}

	// parse solution metadata
	z = fscanf(f, "%d %d %d %zd", &s->n, &s->w, &s->v, &s->size);
	if (z != 4) {
		fprintf(stderr, "read_solution: fscanf(1)\n");
		goto err;
	}

	// parse solutions chosen in solution
	sol_items = calloc(sizeof(struct item), s->n);
	if (sol_items == NULL) {
		fprintf(stderr, "read_solution: calloc(1)\n");
		goto err;
	}
	for (i = 0; i < s->n; i++) {
		z = fscanf(f, "%d %d", &sol_items[i].w, &sol_items[i].v);
	}
	s->items = sol_items;

	z = fscanf(f, "%d %d", N, W);
	if (z != 2) {
		fprintf(stderr, "read_solution: fscanf(2)\n");
		goto err;
	}

	// parse original instance
	*items = calloc(*N, sizeof(struct item));
	if (items == NULL) {
		fprintf(stderr, "read_solution: calloc(2)\n");
		goto err;
	}
	for(i = 0; i < *N; i++) {
		tmp = &(*items)[i];

		z = fscanf(f, "%d %d", &_w, &_v);
		if (z != 2) {
			fprintf(stderr, "read_solution: Error original instance, item %d\n", i+1);
			_w = 0, _v = 0;
		}

		*tmp = new_item(_w, _v);
	}

	fclose(f);
	*sol = s;

	return 0;
err:
	if (f != NULL) fclose(f);
	if (s != NULL) free(s);
	if (sol_items != NULL) free(sol_items);
	if (items != NULL) free(items);

	return 1;
}

/**
 * Write solution and original instance
 *
 * If `name` is NULL, write to `stdout`, try to write file with that name
 *
 * @param name char*
 * @param s struct sack*
 * @return void
 */
void write_solution(char *name, struct sack *s, int N, int W, struct item *items)
{
	FILE *fout;
	int i;

	if (name == NULL)
		fout = stdout;
	else
		fout = fopen(name, "wb");

	if (fout != NULL) {
		printf("Writing solution to %s", name ? name : "stdout\n");

		// write solution
		fprintf(fout, "%d %d %d %zd\n", s->n, s->w, s->v, s->size);
		for (i = 0; i < s->n; i++) {
			fprintf(fout, "%d %d\n", s->items[i].w, s->items[i].v);
		}

		// write original instance
		fprintf(fout, "%d %d\n", N, W);
		for (i = 0; i < N; i++)
			fprintf(fout, "%d %d\n", items[i].w, items[i].v);

		puts(" DONE!");
		if (name != NULL) fclose(fout);
	}
}

#endif
