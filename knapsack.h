#ifndef KNAPH
#define KNAPH

int AVG_KNAP_SIZE = 5;

struct item {
	int w;	// weight
	int v;	// value
};

struct sack {
	int n,				// qty. of items inside sack
		w,			// sack's current weight
		v;			// sack's current value
	size_t size;			// sack's total capacity
	struct item *items;		// array of items
};

void set_avg_knap_size(int new);

struct item new_item(int w, int v);

struct sack *new_sack();

int add_sack_item(struct sack *sack, struct item *item);

void sort_val_desc(struct item *items, int n);

void sort_val_asc(struct item *items, int n);

void sort_weight_desc(struct item *items, int n);

void sort_weight_asc(struct item *items, int n);

void print_items(struct item *items, int n);

void print_sack(struct sack *_sack);

#include "knapsack.c"

#endif // KNAPH
