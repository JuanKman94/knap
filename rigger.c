#include <stdio.h>
#include <stdlib.h>
#include "knapsack.c"
#include "rig.c"

void print_help(char *name)
{
	printf("Usage:\n"
			"\t%s <N> <w_min> <w_max> <v_min> <v_max>\n"
			"Where:\n"
			"\t<N> is the number of items to generate\n"
			"\t<w_min> is the minimum weight for each item, <w_max> the maximum\n"
			"\t<v_min> is the minimum value for each item, <v_max> the maximum\n\n"
			"W is generated according to the w_min and w_max\n"
			, name);
}

int main(int argc, char *argv[])
{
	int N,
		w_min, w_max,
		v_min, v_max;
	char *endptr;

	if (argc < 6) {
		print_help(argv[0]);
		return 1;
	}

	N = strtol(argv[1], &endptr, 10), endptr = NULL;
	w_min = strtol(argv[2], &endptr, 10), endptr = NULL;
	w_max = strtol(argv[3], &endptr, 10), endptr = NULL;
	v_min = strtol(argv[4], &endptr, 10), endptr = NULL;
	v_max = strtol(argv[5], &endptr, 10), endptr = NULL;

	srand(N);

	write_instance(N, w_min, w_max, v_min, v_max);

	return 0;
}
