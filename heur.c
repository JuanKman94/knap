#ifndef KNAP_HEUR
#define KNAP_HEUR

#include <string.h>
#include "knapsack.c"

/**
 * Biggest value first heuristic.
 * 
 * The items are sorted by their value in descending order,
 * then they're put into the sack as they fit.
 *
 * @param struct item* _items
 * @param int n
 * @return struct sack*|NULL
 */
struct sack *heur_value_desc(const struct item *items, int n)
{
	struct item *_items = NULL;
	struct sack *_sack = NULL;
	size_t sz;
	int i,
		z;

	_sack = new_sack();
	sz = sizeof(struct item) * n;
	_items = malloc(sz);

	if (_items == NULL || _sack == NULL) {
		perror("heur_value_desc: malloc()");
		return NULL;
	}
	memcpy(_items, items, sz);

	sort_val_desc(_items, n);
	for (i = 0; i < n; i++) {
		if (_sack->w + _items[i].w <= _sack->size) {
			z = add_sack_item(_sack, &_items[i]);

			if (z != 0) {
				perror("heur_value_desc: add_sack_item()");
			}
		}
	}

	// free memory sorted items
	free(_items);

	return _sack;
}

#endif
