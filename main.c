#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "knapsack.h"
#include "heur.c"
#include "rig.c"

extern int AVG_KNAP_SIZE;

int treat_instance(char *fin, char *fout);
int treat_solution(char *fname);

int main(int argc, char* argv[])
{
	char *out = NULL;

	if (argc < 2) {
		puts("No instance filename specified");
		exit(EXIT_FAILURE);
	}
	// should we write solution to a file?
	if (argc > 2)
		out = argv[2];

	// are we reading a solution?
	if ( (strstr(argv[1], "sol")) != NULL ){
		return treat_solution(argv[1]);
	} else {
		return treat_instance(argv[1], out);
	}


	exit(EXIT_SUCCESS);
}

int treat_instance(char *fin, char *fout)
{
	struct item *items = NULL;
	struct sack *sol = NULL;
	int N, W,
		z;	// status

	z = read_instance(fin, &N, &W, &items);

	if (z != 0) {
		puts("Error parsing the instance file");
		return 1;
	}

	printf("N: %d\tW: %d\t\n", N, W);
	// set the average knap size for all consequent sacks
	set_avg_knap_size(W);

	sol = heur_value_desc(items, N);

	if (sol == NULL) {
		fprintf(stderr, "main: heur_value_desc()\n");
	} else {
		write_solution(fout, sol, N, W, items);
	}

	if (items != NULL) free(items);
	if (sol != NULL) free(sol);

	return 0;
}

int treat_solution(char *fname)
{
	struct item *items = NULL;
	struct sack *sol = NULL;
	int N, W,
		z;	// status

	z = read_solution(fname, &N, &W, &sol, &items);

	if (z != 0) {
		fprintf(stderr, "main: error reading solution\n");
	} else {
		// for now just write solution & items to stdout
		write_solution(NULL, sol, N, W, items);
	}

	if (items != NULL) free(items);
	if (sol != NULL) free(sol);

	return 0;
}
